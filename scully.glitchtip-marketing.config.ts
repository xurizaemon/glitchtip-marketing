import '@scullyio/scully-plugin-puppeteer';
import { getSitemapPlugin } from '@gammastream/scully-plugin-sitemap';
import { getFlashPreventionPlugin } from '@scullyio/scully-plugin-flash-prevention';
import { setPluginConfig, ScullyConfig } from '@scullyio/scully';

const SitemapPlugin = getSitemapPlugin();
setPluginConfig(SitemapPlugin, {
  urlPrefix: 'https://glitchtip.com',
});

export const config: ScullyConfig = {
  projectRoot: './src',
  projectName: 'glitchtip-marketing',
  puppeteerLaunchOptions: {
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
    ],
  },
  defaultPostRenderers: [getFlashPreventionPlugin()],
  outDir: './dist/static',
  routes: {
    '/sdkdocs/:sdkdocs': {
      type: 'contentFolder',
      sdkdocs: {
        folder: './glitchtip-frontend/src/sdk-docs',
      },
    },
    '/legal/:legal': {
      type: 'contentFolder',
      legal: {
        folder: './legal',
      },
    },
    '/blog/:slug': {
      type: 'contentFolder',
      slug: {
        folder: './blog',
      },
    },
    '/documentation/:slug': {
      type: 'contentFolder',
      slug: {
        folder: './documentation',
      },
    },
  },
};
